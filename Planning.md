Liste des actions :
=====
1. Le 31/08/2023 au 04/09/2023 :
     * Cr�er le nouveau projet dans le dossier /groups/brassica/Pangenomes et y ajouter les liens symboliques des fichiers prot�ines.
     * Lire et interpr�ter le script de Fabrice qui permet de transformer les fichiers GFF et fasta des prots en fichiers bed et fasta pour GENESPACE (une seule prot par g�ne avec le nom du g�ne qui est dans le bed).
     * Lecture de la documentation de GENESPACE.
2. Le 05/09/2023 au 08/09/2023 :
     * R�union d��quipe sur l'avancement de mon travail.
     * Lecture de la documentation de Panache (visualisateur et navigateur graphique) (https://github.com/SouthGreenPlatform/panache)
     * Utiliser le site en ligne Panache (https://panache.ird.fr/) avec quelques fichiers au format TSV.
     * Etudier les fichiers de sortie de GENESPACE.
3. Le 11/09/2023 :
     * Etudier la mani�re de charger dans Panache les donn�es (formats d'entr�e) et �tudier la compatibilit� avec les sorties de GENESPACE.
     * D�velopp� un programme de test python-pandas qui convertit les sorties de GENESPACE au format d'entr�e Panache.
4. Le 12/09/2023 :
     * R�union d��quipe sur l'avancement de mon travail.
     * Les actions � r�aliser :
         * Cr�ation d'une machine virtuelle sous genouest "BrasPanache" sous laquelle je vais installer et utiliser Panache.
         * Acc�der et interpr�ter les fichiers de sorties de GENESPACE g�n�r�s par Fabrice.
         * Enregistrer et m�moriser les lignes de commandes linux utilis�es par Fabrice.
         * Ecrire un algorithme qui d�coupe les fichiers de sorties de GENESPACE pour �tre ensuite utilis�s sous Panache.
     * Cr�ation d'une machine virtuelle sous genouest "BrasPanache".
5. Du 13/09/2023 au 25/09/2023 :
     * Acc�der et interpr�ter les fichiers de sorties de GENESPACE g�n�r�s par Fabrice.
     * Enregistrer et m�moriser les lignes de commandes linux utilis�es par Fabrice.
     * Ecrire un algorithme qui d�coupe les fichiers de sorties de GENESPACE pour �tre ensuite utilis�s sous Panache.
     * Installer le logiciel StarUML pour r�aliser des diagrammes.
     * Se former � l'utilisation via des vid�os au logiciel StarUML.
     * R�aliser un mod�le du programme qui d�coupe les fichiers de sorties de GENESPACE pour �tre ensuite utilis�s sous Panache.
6. Le 26/09/2023 :
     * R�union d��quipe sur l'avancement de mon travail.
