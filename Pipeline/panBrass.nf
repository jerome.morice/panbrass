nextflow.enable.dsl=2

checkProteinOutputDir = "$params.output/CheckProtein"
prepareGenespaceOutputDir = "$params.output/PrepareGenespace"
orthoFinderOutputDir = "$params.output/OrthoFinder"
genespaceOutputDir = "$params.output/Genespace"
formattingGenespaceIntoPanacheOutputDir = "$params.output/FormattingGenespaceIntoPanache"
prepareChrSizeOutputDir = "$params.output/PrepareChrSize"

params.pangenomeFileName = "pangenome_*.tsv"

process prepareGenespace {
    debug true
    input:
    tuple val(nameInputChannel), file(gffFileInputChannel), file(proteinFastaFileInputChannel), val(typeInputChannel), file(correspondanceChromosomeFileInputChannel)

    output:
    path "${nameInputChannel}.{check.fa,gs.fa,gs.bed,pkl.bed}"

    // copy the checkProtein output files located in the work folder and paste them in a folder of my choice
    //publishDir "$checkProteinOutputDir", mode: 'copy'
    publishDir "$checkProteinOutputDir", mode: 'copy', pattern: '*.check.fa'
    // copy the prepareGenespace output files located in the work folder and paste them in a folder of my choice
    //publishDir "$prepareGenespaceOutputDir", mode: 'copy'
    publishDir "$prepareGenespaceOutputDir", mode: 'copy', pattern: '*.gs.fa'
    publishDir "$prepareGenespaceOutputDir", mode: 'copy', pattern: '*.gs.bed'
    publishDir "$prepareGenespaceOutputDir", mode: 'copy', pattern: '*.pkl.bed'

    script:
    """
    #echo $nameInputChannel $gffFileInputChannel $proteinFastaFileInputChannel $typeInputChannel $correspondanceChromosomeFileInputChannel
    #head -5 $gffFileInputChannel
    #head -5 $proteinFastaFileInputChannel
    #head -5 $correspondanceChromosomeFileInputChannel
    python3 /panbrass/scripts/CheckProtein/src/check_prots.py --fasta $proteinFastaFileInputChannel --output_fasta ${nameInputChannel}.check.fa
    python3 /panbrass/scripts/PrepareGenespace/src/prepareGENESPACE.py --fasta ${nameInputChannel}.check.fa --gff $gffFileInputChannel --fasta_output ${nameInputChannel}.gs.fa --bed4_output ${nameInputChannel}.gs.bed --bed6_output ${nameInputChannel}.pkl.bed --type $typeInputChannel --rename $correspondanceChromosomeFileInputChannel
    """
}

process orthoFinder {
    cpus params.cpus_orthoFinder
    memory params.memory_orthoFinder

    debug true
    input:
    path fastaFileInputChannel

    output:
    path "WorkingDirectory/orthofinder/output/**"
    //path "**"

    // copy the orthoFinder output files located in the work folder and paste them in a folder of my choice
    //bug : publishDir "$orthoFinderOutputDir", mode: 'copy', pattern: '!WorkingDirectory/orthofinder/output/Results*/WorkingDirectory/*'
    publishDir "$orthoFinderOutputDir", mode: 'copy', saveAs: { filename ->
        if (!filename.contains('/WorkingDirectory/')) {
            return filename
        }
        return null
    }

    script:
    """
    mkdir WorkingDirectory
    mkdir WorkingDirectory/orthofinder
    mkdir WorkingDirectory/orthofinder/input

    echo ${fastaFileInputChannel}

    for fastaFile in ${fastaFileInputChannel}; do
        echo "\${fastaFile}"
        #ln -s "\${fastaFile}" WorkingDirectory/orthofinder/input/"\${fastaFile}"
        #cp "\${fastaFile}" WorkingDirectory/orthofinder/input/"\${fastaFile}"
        mv "\${fastaFile}" WorkingDirectory/orthofinder/input/"\${fastaFile}"
    done
    #mkdir WorkingDirectory/orthofinder/output

    # -f : fasta files directory
    # -t : number of threads
    # -a : number of orthofinder threads
    # -X : to not add species names to sequence names
    # -o : output filename without any directory path
    #/panbrass/OrthoFinder/OrthoFinder/orthofinder -f WorkingDirectory/orthofinder/input -t ${task.cpus} -a 1 -X -o WorkingDirectory/orthofinder/output --fewer-files
    /panbrass/OrthoFinder/OrthoFinder/orthofinder -f WorkingDirectory/orthofinder/input -t ${task.cpus} -a 1 -X -o WorkingDirectory/orthofinder/output

    rm -r WorkingDirectory/orthofinder/output/Results*/Orthogroup_Sequences
    rm -r WorkingDirectory/orthofinder/output/Results*/Resolved_Gene_Trees
    rm -r WorkingDirectory/orthofinder/output/Results*/Gene_Trees
    """
}

process genespace1 {
    debug true
    input:
    path fastaFileInputChannel
    path bedFileInputChannel
    path orthoFinderInputChannel

    output:
    path "WorkingDirectory/*"

    script:
    """
    mkdir WorkingDirectory
    mkdir WorkingDirectory/genespace
    mkdir WorkingDirectory/genespace/peptide
    mkdir WorkingDirectory/genespace/bed
    mkdir WorkingDirectory/genespace/orthofinder
    for fastaFile in ${fastaFileInputChannel}; do
        echo "\${fastaFile}"
        #ln -s "\${fastaFile}" WorkingDirectory/genespace/peptide/"\${fastaFile}"
        #cp "\${fastaFile}" WorkingDirectory/genespace/peptide/"\${fastaFile}"
        mv "\${fastaFile}" WorkingDirectory/genespace/peptide/"\${fastaFile}"
    done
    for bedFile in ${bedFileInputChannel}; do
        echo "\${bedFile}"
        #ln -s "\${bedFile}" WorkingDirectory/genespace/bed/"\${bedFile}"
        #cp "\${bedFile}" WorkingDirectory/genespace/bed/"\${bedFile}"
        mv "\${bedFile}" WorkingDirectory/genespace/bed/"\${bedFile}"
    done
    echo ${orthoFinderInputChannel}
    #ln -s ${orthoFinderInputChannel} WorkingDirectory/genespace/orthofinder
    #cp -r ${orthoFinderInputChannel} WorkingDirectory/genespace/orthofinder
    mv ${orthoFinderInputChannel} WorkingDirectory/genespace/orthofinder
    """
}

process genespace2 {
    memory params.memory_genespace
    queue = params.queue_genespace

    debug true
    input:
    path genespaceInputChannel

    output:
    path "genespace/**"
    //path "**"

    // copy the genespace output files located in the work folder and paste them in a folder of my choice
    publishDir "$genespaceOutputDir", mode: 'copy', pattern: 'WorkingDirectory/**/Genespace/genespace/pangenes/**'
    publishDir "$genespaceOutputDir", mode: 'copy', pattern: 'WorkingDirectory/**/Genespace/genespace/riparian/**'
    publishDir "$genespaceOutputDir", mode: 'copy', pattern: 'WorkingDirectory/**/Genespace/genespace/results/*.{csv,rda,tsv,txt}'

    script:
    """
    #!/usr/bin/env Rscript
    library(GENESPACE)
    gpar <- init_genespace(
        wd = "genespace",
        path2mcscanx = "/panbrass/MCScanX/"
    )
    gpar <- run_genespace(gsParam = gpar)
    """
}

process formattingGenespaceIntoPanache {
    debug true
    input:
    path genespaceInputChannel

    output:
    path "WorkingDirectory/*"

    // copy the orthoFinder output files located in the work folder and paste them in a folder of my choice
    publishDir "$formattingGenespaceIntoPanacheOutputDir", mode: 'copy'

    script:
    """
    mkdir WorkingDirectory
    mkdir WorkingDirectory/formattingGenespaceIntoPanache
    mkdir WorkingDirectory/formattingGenespaceIntoPanache/input

    #cp ${genespaceInputChannel}/results/syntenicBlock_coordinates.csv WorkingDirectory/formattingGenespaceIntoPanache/input/syntenicBlock_coordinates.csv
    mv ${genespaceInputChannel}/results/syntenicBlock_coordinates.csv WorkingDirectory/formattingGenespaceIntoPanache/input/syntenicBlock_coordinates.csv
    mkdir WorkingDirectory/formattingGenespaceIntoPanache/output

    python3 /panbrass/scripts/FormattingGenespaceOutputFileIntoPanacheInputFile/V1/src/Main.py WorkingDirectory/formattingGenespaceIntoPanache/input/syntenicBlock_coordinates.csv WorkingDirectory/formattingGenespaceIntoPanache/output/${params.pangenomeFileName.replaceFirst(/\*/, "%s")}
    """
}

process prepareChrSize {
    debug true
    input:
    tuple val(nameInputChannel), file(correspondanceChromosomeFileInputChannel), file(chrFastaFileInputChannel)

    output:
    path "${nameInputChannel}.chr.size.txt"

    // copy the prepareChrSize output files located in the work folder and paste them in a folder of my choice
    //publishDir "$prepareChrSizeOutputDir", mode: 'copy'
    publishDir "$prepareChrSizeOutputDir", mode: 'copy', pattern: '*.chr.size.txt'

    script:
    """
    #echo $nameInputChannel $correspondanceChromosomeFileInputChannel $chrFastaFileInputChannel
    #head -5 $correspondanceChromosomeFileInputChannel
    #head -5 $chrFastaFileInputChannel
    python3 /panbrass/scripts/PrepareChrSizeFile/src/PrepareChrSizeFile.py --fasta $chrFastaFileInputChannel --rename $correspondanceChromosomeFileInputChannel --chr_size_output ${nameInputChannel}.chr.size.txt
    """
}

workflow {
    sampleFileInputChannel1 = Channel
	.fromPath(params.sampleFile)
	.splitCsv(header:true)
	.map {row-> tuple(row.name, file(row.gff_file), file(row.prot_file), row.type, file(row.correspondance_chromosome))}
	.view()
    
    fastaBedFileOutputChannel = prepareGenespace(sampleFileInputChannel1)
	.view()

    gsBedFileOutputChannel = fastaBedFileOutputChannel
	.map{T->[T[1]]}
	.collect()
	.view()

    pklBedFileOutputChannel = fastaBedFileOutputChannel
	.map{T->[T[2]]}
	.collect()
	.view()

    gsFastaFileOutputChannel = fastaBedFileOutputChannel
	.map{T->[T[3]]}
	.collect()
	.view()

    orthoFinderOutputChannel = orthoFinder(gsFastaFileOutputChannel)
    	.view()

    if (params.genespace) {
	genespaceOutputChannel1 = genespace1(gsFastaFileOutputChannel, gsBedFileOutputChannel, orthoFinderOutputChannel)
	    .view()

	genespaceOutputChannel2 = genespace2(genespaceOutputChannel1)
	    .view()

	formattingGenespaceIntoPanache(genespaceOutputChannel2)
	    .view()
    }

    sampleFileInputChannel2 = Channel
	.fromPath(params.sampleFile)
	.splitCsv(header:true)
	.map {row-> tuple(row.name, file(row.correspondance_chromosome), file(row.chromosome_fasta_file))}
	.view()
    
    chrSizeFileOutputChannel = prepareChrSize(sampleFileInputChannel2)
	.view()

    toto = chrSizeFileOutputChannel
	.map{T->[T[0]]}
	.collect()
	.view()
}
