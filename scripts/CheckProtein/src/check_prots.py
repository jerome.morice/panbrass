from Bio import SeqIO
import argparse
import sys
import re

# Recuperation des parametres
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--fasta", required=True, help="fasta file")
ap.add_argument("-of", "--output_fasta", required=True, help="output fasta file")
args = vars(ap.parse_args())

of=open(args['output_fasta'],'w')
for record in SeqIO.parse(args['fasta'], "fasta"):
    record.description=''
    if ((record.seq[-1]=='.') or (record.seq[-1] == '*')):
        record.seq=record.seq[:-1]
    if ('.' in record.seq):
        print("Invalid seq '" + record.id + "' : " + record.seq, file=sys.stdout)
    else:
        SeqIO.write(record, of, "fasta")
