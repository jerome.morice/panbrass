class Block(object):
    def __init__(self, chromosome=None, start=None, end=None, orient=None):
        self.chromosome = chromosome
        self.start = start
        self.end = end
        self.orient = orient # "+" (non reversed orientation) ou "-" (reversed orientation)

    def getChromosome(self):
        return self.chromosome

    def setChromosome(self, chromosome):
        self.chromosome = chromosome
    
    def getStart(self):
        return self.start
    
    def setStart(self, start):
        self.start = start

    def getEnd(self):
        return self.end
    
    def setEnd(self, end):
        self.end = end

    def getOrient(self):
        return self.orient
    
    def setOrient(self, orient):
        self.orient = orient
        
    def toString(self):
        toString = "genome: " + ("" if (self.chromosome is None) else ("" if (self.chromosome.getGenome() is None) else self.chromosome.getGenome().getName()))
        toString += ", chromosome: " + ("" if (self.chromosome is None) else self.chromosome.getName())
        toString += ", start: " + ("" if (self.start is None) else str(self.start))
        toString += ", end: " + ("" if (self.end is None) else str(self.end))
        toString += ", orient: " + ("" if (self.orient is None) else str(self.orient))
        return toString