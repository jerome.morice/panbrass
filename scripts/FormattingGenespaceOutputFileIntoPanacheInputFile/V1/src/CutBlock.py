class CutBlock(object):
    def __init__(self, chromosome=None, start=None, end=None, genomesPresent=None, pairwiseBlocksPresent=None):
        self.chromosome = chromosome
        self.start = start
        self.end = end
        self.genomesPresent = genomesPresent
        self.pairwiseBlocksPresent = pairwiseBlocksPresent

    def getChromosome(self):
        return self.chromosome

    def setChromosome(self, chromosome):
        self.chromosome = chromosome
    
    def getStart(self):
        return self.start
    
    def setStart(self, start):
        self.start = start

    def getEnd(self):
        return self.end
    
    def setEnd(self, end):
        self.end = end
        
    def getGenomesPresent(self):
        return self.genomesPresent

    def setGenomesPresent(self, genomesPresent):
        self.genomesPresent = genomesPresent

    def getPairwiseBlocksPresent(self):
        return self.pairwiseBlocksPresent

    def setPairwiseBlocksPresent(self, pairwiseBlocksPresent):
        self.pairwiseBlocksPresent = pairwiseBlocksPresent

    def toString(self):
        toString = "genome: " + ("" if (self.chromosome is None) else ("" if (self.chromosome.getGenome() is None) else self.chromosome.getGenome().getName()))
        toString += ", chromosome: " + ("" if (self.chromosome is None) else self.chromosome.getName())
        toString += ", start: " + ("" if (self.start is None) else str(self.start))
        toString += ", end: " + ("" if (self.end is None) else str(self.end))
        toString += ", genomesPresent: "
        if (self.genomesPresent is not None):
            for genomePresent in self.genomesPresent:
                toString += genomePresent.toString() + " "
        toString += ", pairwiseBlocksPresent: "
        if (self.pairwiseBlocksPresent is not None):
            for pairwiseBlockPresent in self.pairwiseBlocksPresent:
                toString += pairwiseBlockPresent.toString() + " "
        return toString