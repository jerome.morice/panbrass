class PairwiseBlock(object):
    def __init__(self, id=None, block1=None, block2=None):
        self.id = id
        self.block1 = block1
        self.block2 = block2

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id
     
    def getBlock1(self):
        return self.block1

    def setBlock1(self, block1):
        self.block1 = block1

    def getBlock2(self):
        return self.block2

    def setBlock2(self, block2):
        self.block2 = block2

    def toString(self):
        toString = "id: " + ("" if (self.id is None) else self.id)
        toString += ", block1: " + ("" if (self.block1 is None) else self.block1.toString())
        toString += ", block2: " + ("" if (self.block2 is None) else self.block2.toString())
        return toString