class Chromosome(object):
    def __init__(self, name=None, genome=None, blocks=None, cutBlocks=None):
        self.name = name
        self.genome = genome
        self.blocks = blocks
        self.cutBlocks = cutBlocks
        
    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getGenome(self):
        return self.genome

    def setGenome(self, genome):
        self.genome = genome

    def getBlocks(self):
        return self.blocks

    def setBlocks(self, blocks):
        self.blocks = blocks

    def getCutBlocks(self):
        return self.cutBlocks

    def setCutBlocks(self, cutBlocks):
        self.cutBlocks = cutBlocks

    def toString(self):
        toString = "name: " + ("" if (self.name is None) else self.name)
        toString += ", genome: " + ("" if (self.genome is None) else self.genome.getName())
        toString += ", blocks: "
        if (self.blocks is not None):
            for block in self.blocks:
                toString += "start: " + block.getStart() + " end: " + block.getEnd() + " orient: " + block.getOrient() + " "
        toString += ", cutBlocks: "
        if (self.cutBlocks is not None):
            for cutBlock in self.cutBlocks:
                toString += "start: " + cutBlock.getStart() + " end: " + cutBlock.getEnd() + " "
        return toString