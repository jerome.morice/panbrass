class Genome(object):
    def __init__(self, name=None, chromosomes=None):
        self.name = name
        self.chromosomes = chromosomes
        
    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name
        
    def getChromosomes(self):
        return self.chromosomes

    def setChromosomes(self, chromosomes):
        self.chromosomes = chromosomes
                
    def toString(self):
        toString = "name: " + ("" if (self.name is None) else self.name)
        toString += ", chromosomes: "
        if (self.chromosomes is not None):
            for chromosome in self.chromosomes:
                toString += chromosome.getName() + " "
        return toString