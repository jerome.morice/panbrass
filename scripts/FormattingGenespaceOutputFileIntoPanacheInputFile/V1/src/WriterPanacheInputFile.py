class WriterPanacheInputFile(object):
    SEPARATOR_COLUMN            =   "\t"
    
    def __init__(self):
        '''
        Constructor
        '''

    def write(self, argv, genomes):
        FILE_PANGENOME = argv[1] #"pangenome_%s.tsv"
        
        # for each genome 1
        for genome1 in genomes:
            # we create a new file which bears the name of this genome
            filePangenome = open(FILE_PANGENOME % genome1.getName(), 'w')
            # we write the 1st line of this file
            filePangenome.write("#Chromosome" + self.SEPARATOR_COLUMN + "FeatureStart" + self.SEPARATOR_COLUMN + "FeatureStop" + self.SEPARATOR_COLUMN + "Sequence_IUPAC_Plus" + self.SEPARATOR_COLUMN + "SimilarBlocks" + self.SEPARATOR_COLUMN + "Fonction")
            # to this 1st line we add the columns of the genomes 2
            for genome in genomes:
                if (genome != genome1):
                    filePangenome.write(self.SEPARATOR_COLUMN + genome.getName())
            filePangenome.write("\n")
            # for each chromosome of genome 1
            for chromosomeOfGenome1 in genome1.getChromosomes():
                # for each split of the chromosome
                for cutBlock in chromosomeOfGenome1.getCutBlocks():
                    # we write the chromosome name, the start and the end of this split
                    filePangenome.write(cutBlock.getChromosome().getName() + self.SEPARATOR_COLUMN + str(cutBlock.getStart()) + self.SEPARATOR_COLUMN + str(cutBlock.getEnd()) + self.SEPARATOR_COLUMN + "." + self.SEPARATOR_COLUMN)
                    # in the 'SimilarBlocks' column we write the chromosome name, the start and the end of the blocks 1 overlapping this split
                    for index in range(len(cutBlock.getPairwiseBlocksPresent())):
                        pairwiseBlockPresent = cutBlock.getPairwiseBlocksPresent()[index]
                        if (index > 0):
                            filePangenome.write(";")
                        filePangenome.write(pairwiseBlockPresent.getBlock1().getChromosome().getName() + ":" + str(pairwiseBlockPresent.getBlock1().getStart()) + ":" + str(pairwiseBlockPresent.getBlock1().getEnd()))
                    filePangenome.write(self.SEPARATOR_COLUMN + ".")
                    # in the genomes 2 columns: we write 1 (if genome 2 is present in this split) or 0 (if genome 2 is not present in this split)
                    for genome in genomes:
                        if (genome != genome1):
                            filePangenome.write(self.SEPARATOR_COLUMN + ("1" if (genome in cutBlock.getGenomesPresent()) else "0"))
                    filePangenome.write("\n")
            filePangenome.close()