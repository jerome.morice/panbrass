import sys

import pandas

from Block import Block
from Chromosome import Chromosome
from CutBlock import CutBlock
from Genome import Genome
from PairwiseBlock import PairwiseBlock

from WriterPanacheInputFile import WriterPanacheInputFile

DICT_CHROMOSOME_NAMES_TO_MODIFY    =    {"A1": "A01", "A2": "A02", "A3": "A03", "A4": "A04", "A5": "A05", "A6": "A06", "A7": "A07", "A8": "A08", "A9": "A09", "C1": "C01", "C2": "C02", "C3": "C03", "C4": "C04", "C5": "C05", "C6": "C06", "C7": "C07", "C8": "C08", "C9": "C09"}
    
def main(argv):
    FILE_INPUT_GENESPACE = argv[0] #"syntenicBlock_coordinates.txt"
    try:
        # reading the coordinates file of the syntenic blocks
        dfGenespace = pandas.read_csv(FILE_INPUT_GENESPACE, sep=',')#, index_col=0)
        for chromosomeNamesToModify in DICT_CHROMOSOME_NAMES_TO_MODIFY.keys():
            dfGenespace.loc[dfGenespace["chr1"] == chromosomeNamesToModify, "chr1"] = DICT_CHROMOSOME_NAMES_TO_MODIFY[chromosomeNamesToModify]
            dfGenespace.loc[dfGenespace["chr2"] == chromosomeNamesToModify, "chr2"] = DICT_CHROMOSOME_NAMES_TO_MODIFY[chromosomeNamesToModify]
        if (len(dfGenespace[dfGenespace["startBp1"] >= dfGenespace["endBp1"]]) > 0):
            raise Exception("Le 'startBp1' est superieur au 'endBp1' une ou plusieurs fois dans le fichier '" + FILE_INPUT_GENESPACE + "' !")
        dfGenespace["minStartEndBp2"] = dfGenespace.apply(lambda row: row["startBp2"] if (row["startBp2"] <= row["endBp2"]) else row["endBp2"], axis = 1)
        dfGenespace["maxStartEndBp2"] = dfGenespace.apply(lambda row: row["startBp2"] if (row["startBp2"] > row["endBp2"]) else row["endBp2"], axis = 1)
        # delete lines when block 1 = block 2
        dfGenespace = dfGenespace.loc[~((dfGenespace["genome1"] == dfGenespace["genome2"]) & (dfGenespace["chr1"] == dfGenespace["chr2"]) & (dfGenespace["startBp1"] == dfGenespace["minStartEndBp2"]) & (dfGenespace["endBp1"] == dfGenespace["maxStartEndBp2"]))]
    
        # we search for the different unique blocks
        dictBlocks = {}
        dfBlocks = dfGenespace[["genome1", "chr1", "startBp1", "endBp1"]].copy()
        dfBlocks.rename(columns = {"genome1":"genome", "chr1":"chr", "startBp1":"startBp", "endBp1":"endBp"}, inplace = True)
        dfBlocks = dfBlocks.drop_duplicates()
        
        # we create the different genomes
        genomes = []
        genomeNames = dfBlocks["genome"].unique()
        for genomeName in genomeNames:
            genome = Genome(genomeName, [])
            # we create the different chromosomes of this genome
            dfBlocksOfGenome = dfBlocks[dfBlocks["genome"] == genome.getName()]
            chromosomeNamesOfGenome = dfBlocksOfGenome["chr"].unique()
            for chromosomeNameOfGenome in chromosomeNamesOfGenome:
                chromosomeOfGenome = Chromosome(chromosomeNameOfGenome, genome, [], [])
                # we create the different blocks of this chromosome
                dfBlocksOfChromosome = dfBlocksOfGenome[dfBlocksOfGenome["chr"] == chromosomeOfGenome.getName()]
                for index, row in dfBlocksOfChromosome.iterrows():
                    block = Block(chromosomeOfGenome, row["startBp"], row["endBp"], None)#row["orient"])
                    dictBlocks[id(block)] = block
                    dfGenespace.loc[(dfGenespace["genome1"] == genome.getName()) & (dfGenespace["chr1"] == chromosomeOfGenome.getName()) & (dfGenespace["startBp1"] == block.getStart()) & (dfGenespace["endBp1"] == block.getEnd()), "idBlock1"] = id(block)
                    dfGenespace.loc[(dfGenespace["genome2"] == genome.getName()) & (dfGenespace["chr2"] == chromosomeOfGenome.getName()) & (dfGenespace["minStartEndBp2"] == block.getStart()) & (dfGenespace["maxStartEndBp2"] == block.getEnd()), "idBlock2"] = id(block)
                    chromosomeOfGenome.getBlocks().append(block)
                genome.getChromosomes().append(chromosomeOfGenome)
            genomes.append(genome)    
        
        # for each genome 1
        for genome1 in genomes:
            dfGenespaceOfGenome1 = dfGenespace[dfGenespace["genome1"] == genome1.getName()]
            # for each chromosome of genome 1
            for chromosomeOfGenome1 in genome1.getChromosomes():
                # we create the pairs of blocks
                pairwiseBlocks = []
                dfGenespaceOfGenome1AndChr1 = dfGenespaceOfGenome1[dfGenespaceOfGenome1["chr1"] == chromosomeOfGenome1.getName()]
                for index, row in dfGenespaceOfGenome1AndChr1.iterrows():
                    block1 = dictBlocks[row["idBlock1"]]
                    block2 = dictBlocks[row["idBlock2"]]
                    pairwiseBlock = PairwiseBlock(row["blkID"], block1, block2)
                    pairwiseBlocks.append(pairwiseBlock)
                # we group the overlapping blocks 1
                while (True):
                    # we create a group of empty overlapping blocks 1
                    overlappingPairwiseBlocks = []
                    firstPairwiseBlock = pairwiseBlocks[0]
                    minStartOverlappingBlock1s = firstPairwiseBlock.getBlock1().getStart()
                    maxEndOverlappingBlock1s = firstPairwiseBlock.getBlock1().getEnd()
                    # we add to this group the 1st block 1
                    overlappingPairwiseBlocks.append(firstPairwiseBlock)
                    pairwiseBlocks.remove(firstPairwiseBlock)
                    # as long as there remains at least one block 1 which overlaps this group then we add them
                    while (True):
                        nbOverlappingPairwiseBlocksAdded = 0
                        for pairwiseBlock in pairwiseBlocks:
                            if ((pairwiseBlock.getBlock1().getStart() <= maxEndOverlappingBlock1s) & (pairwiseBlock.getBlock1().getEnd() >= minStartOverlappingBlock1s)):
                                minStartOverlappingBlock1s = min(minStartOverlappingBlock1s, pairwiseBlock.getBlock1().getStart())
                                maxEndOverlappingBlock1s = max(maxEndOverlappingBlock1s, pairwiseBlock.getBlock1().getEnd())
                                overlappingPairwiseBlocks.append(pairwiseBlock)
                                pairwiseBlocks.remove(pairwiseBlock)
                                nbOverlappingPairwiseBlocksAdded += 1
                        if (nbOverlappingPairwiseBlocksAdded == 0):
                            break
                    # we cut this group of overlapping blocks 1 (splitting)
                    startEndPositions = []
                    # we recover the different start and end positions of these overlapping blocks 1
                    for overlappingPairwiseBlock in overlappingPairwiseBlocks:
                        if (overlappingPairwiseBlock.getBlock1().getStart() not in startEndPositions):
                            startEndPositions.append(overlappingPairwiseBlock.getBlock1().getStart())
                        if (overlappingPairwiseBlock.getBlock1().getEnd() not in startEndPositions):
                            startEndPositions.append(overlappingPairwiseBlock.getBlock1().getEnd())
                    # we sort these different start and end positions in ascending order
                    startEndPositions.sort()
                    # we create the different splits
                    for index in range(len(startEndPositions)):
                        if (index < (len(startEndPositions)-1)):
                            # we create a split with as terminals: two consecutive positions of these different start and end positions
                            cutBlock = CutBlock(chromosomeOfGenome1, startEndPositions[index], startEndPositions[index+1], [], [])
                            # we are looking for the blocks 1 of this group which overlap this split
                            for overlappingPairwiseBlock in overlappingPairwiseBlocks:
                                if ((overlappingPairwiseBlock.getBlock1().getStart() <= cutBlock.getStart()) & (overlappingPairwiseBlock.getBlock1().getEnd() >= cutBlock.getEnd())):
                                    cutBlock.getPairwiseBlocksPresent().append(overlappingPairwiseBlock)
                                    # to these blocks 1 of this group which overlap this split, we recover the different genomes associated with blocks 2
                                    if (overlappingPairwiseBlock.getBlock2().getChromosome().getGenome() not in cutBlock.getGenomesPresent()):
                                        cutBlock.getGenomesPresent().append(overlappingPairwiseBlock.getBlock2().getChromosome().getGenome())
                            # we add this split to the list of splits of this chromosome
                            chromosomeOfGenome1.getCutBlocks().append(cutBlock)
                    # once all blocks 1 are part of a group then we have finished the grouping of overlapping blocks 1
                    if (len(pairwiseBlocks) == 0):
                        break
        # we generate the files for pangenome (1 file per genome 1)
        writerPanacheInputFile = WriterPanacheInputFile()
        writerPanacheInputFile.write(argv, genomes)
    except Exception as e:
        print(e)

# manage arguments        
if (__name__ == "__main__"):
    main(sys.argv[1:])