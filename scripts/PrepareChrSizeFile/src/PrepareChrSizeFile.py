from Bio import SeqIO
import gzip
import argparse

# Recuperation des parametres
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--fasta", required=True, help="fasta file")
ap.add_argument("-r", "--rename", required=True, help="correspondance file of chromosomes")
ap.add_argument("-cso", "--chr_size_output", required=True, help="chr size output file")
args = vars(ap.parse_args())

conv={}
rename_file=open(args['rename'],'r')
for line in rename_file:
    (old_name, new_name)=line.strip().split(" ")
    conv[old_name]=new_name

os=open(args['chr_size_output'],'w')
with gzip.open(args['fasta'],'rt') as handle:
    for record in SeqIO.parse(handle, "fasta"):
        if (record.id in conv):
            # On l'ecrit dans le fichier
            os.write(conv[record.id] + " " + str(len(record)) + "\n")
os.close()