import argparse
import pandas as pd
import pickle

from io import BytesIO
from werkzeug.datastructures import FileStorage


ALLOWED_EXTENSIONS = {'tsv', 'bed', 'txt', 'pkl'}

# Liste des 73 couleurs en format hexadecimal utilisees pour les orthogroupes
COLORS_ORTHOGROUPS = [
    '#FF0000', '#00FF00', '#0000FF', '#FFFF00', '#008080', '#FF00FF', '#00FFFF', '#808000', '#FFA500', '#800080',
    '#FFD700', '#000080', '#FFC0CB', '#A52A2A', '#90EE90', '#9370DB', '#F0E68C', '#87CEFA', '#3CB371', '#808080',
    '#800000', '#FF6347', '#4B0082', '#008000', '#4682B4', '#D2691E', '#B0C4DE', '#FF4500', '#DA70D6', '#8B4513',
    '#DC143C', '#2E8B57', '#6A5ACD', '#20B2AA', '#7FFF00', '#B22222', '#5F9EA0', '#FF1493', '#00CED1', '#1E90FF',
    '#FF69B4', '#8A2BE2', '#7CFC00', '#FF7F50', '#6495ED', '#9932CC', '#8B0000', '#E9967A', '#8FBC8F', '#483D8B',
    '#2F4F4F', '#00BFFF', '#696969', '#228B22', '#DCDCDC', '#DAA520', '#ADFF2F', '#CD5C5C', '#ADD8E6', '#F08080',
    '#D3D3D3', '#FFB6C1', '#FFA07A', '#778899', '#32CD32', '#66CDAA', '#0000CD', '#BA55D3', '#00FA9A', '#7B68EE',
    '#48D1CC', '#C71585', '#191970'
]
# COLORS_ORTHOGROUPS = [
#     Red, Green, Blue, Yellow, Teal, Magenta, Cyan, Olive, Orange, Purple,
#     Gold, Navy, Pink, Brown, Light Green, Medium Purple, Khaki, Light Sky Blue, Medium Sea Green, Gray,
#     Maroon, Tomato, Indigo, Dark Green, Steel Blue, Chocolate, Light Steel Blue, Orange Red, Orchid, Saddle Brown,
#     Crimson, Sea Green, Slate Blue, Light Sea Green, Chartreuse, Firebrick, Cadet Blue, Deep Pink, Dark Turquoise, Dodger Blue,
#     Hot Pink, Blue Violet, Lawn Green, Coral, Cornflower Blue, Dark Orchid, Dark Red, Dark Salmon, Dark Sea Green, Dark Slate Blue,
#     Dark Slate Gray, Deep Sky Blue, Dim Gray, Forest Green, Gainsboro, Goldenrod, Green Yellow, Indian Red, Light Blue, Light Coral,
#     Light Gray, Light Pink, Light Salmon, Light Slate Gray, Lime Green, Medium Aquamarine, Medium Blue, Medium Orchid, Medium Spring Green, Medium Slate Blue,
#     Medium Turquoise, Medium Violet Red, Midnight Blue
# ]

# Recuperation des parametres
ap = argparse.ArgumentParser()
ap.add_argument("-tsv", "--tsv", required=True, help="tsv file")
ap.add_argument("-csv", "--csv", required=True, help="csv file")
ap.add_argument("-po", "--pickle_output", required=True, help="pickle output file")
args = vars(ap.parse_args())

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def readTsvFile(tsvFile):
    tsvDf = pd.read_csv(tsvFile, sep='\t')
    # Renomme la 1ere colonne en 'orthogroups'
    tsvDf.columns.values[0] = 'orthogroups'
    # Transforme les colonnes des noms de genomes en ligne
    meltedTsvDf = tsvDf.melt(id_vars=['orthogroups'], var_name='original_genome', value_name='gene')
    # Supprime les couples 'orthogroups'-'original_genome' avec aucun gene
    meltedTsvDf = meltedTsvDf.loc[meltedTsvDf['gene'].isna() == False]
    # Transforme la chaine de caracteres des noms de gene separes par une virgule en plusieurs lignes avec un seul nom de gene a la fois
    explodedMeltedTsvDf = meltedTsvDf.set_index(['orthogroups', 'original_genome']).apply(lambda x: x.str.split(', ').explode()).reset_index()
    # Regroupe les orthogroupes par 'gene'-'original_genome' puis les concatene dans l ordre alphabetique
    groupedExplodedMeltedTsvDf = explodedMeltedTsvDf.groupby(['gene', 'original_genome'])['orthogroups'].apply(lambda x: ', '.join(sorted(x))).reset_index(name='orthogroups')
    # Recupere les differentes orthogroupes
    orthogroupDf = groupedExplodedMeltedTsvDf['orthogroups'].drop_duplicates().to_frame()
    # Associe une couleur aux orthogroupes en etendant la liste de couleurs 'COLORS_ORTHOGROUPS'
    orthogroupDf['color_orthogroups'] = (COLORS_ORTHOGROUPS * (len(orthogroupDf) // len(COLORS_ORTHOGROUPS) + 1))[:len(orthogroupDf)]
    # Fusionne les dataframes 'groupedExplodedMeltedTsvDf' et 'orthogroupDf'
    mergedGroupedExplodedMeltedTsvDf = pd.merge(groupedExplodedMeltedTsvDf, orthogroupDf, on=['orthogroups'])
    # A present 'mergedGroupedExplodedMeltedTsvDf' est de la forme ['original_genome', 'gene', 'orthogroups', 'color_orthogroups']
    # print(mergedGroupedExplodedMeltedTsvDf)

    # Recupere les noms des genomes a partir de la 2eme colonne et cree un dictionnaire de valeurs vides
    bedChrSizeByOriginalGenomeDict = {col: None for col in tsvDf.columns[1:]}
    # Associe a chaque valeur de 'bedChrSizeByOriginalGenomeDict' le dictionnaire '{'bedFileName': None, 'renamedGenome': None, 'bedDf': None, 'chrSizeFileName': None, 'chrSizeDf': None}'
    bedChrSizeByOriginalGenomeDict = {key: {'bedFileName': None, 'renamedGenome': key, 'bedDf': None, 'chrSizeFileName': None, 'chrSizeDf': None} for key in bedChrSizeByOriginalGenomeDict.keys()}
    # Cree un dictionnaire contenant les attributs a inclure dans un fichier Pickle
    pickleDict = {
        'tsvDf': mergedGroupedExplodedMeltedTsvDf,
        # 'bedDf' est de la forme ['original_genome', 'renamed_genome', 'chromosome', 'chromosome_size', 'gene', 'strand', 'original_start', 'original_end', 'flipped_start', 'flipped_end', 'is_real_chr_size', 'should_gene_be_displayed']
        'bedChrSizeByOriginalGenomeDict': bedChrSizeByOriginalGenomeDict,
        # '{'bedFileName': None, 'renamedGenome': None, 'bedDf': None, 'chrSizeFileName': None, 'chrSizeDf': None}'
        'mergedTsvBedDf': None
    }
    return(pickleDict)

def readBedChrSizeFile(originalGenomes, bedFileStates, bedFiles, renamedGenomes, chrSizeFileStates, chrSizeFiles, pickleDict):
    bedDfs = []
    for i, bedFile in enumerate(bedFiles):
        pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['renamedGenome'] = renamedGenomes[i];

        chrSizeDf = None
        chrSizeFile = chrSizeFiles[i]
        if chrSizeFile and chrSizeFile.filename:
            if allowed_file(chrSizeFile.filename):
                chrSizeDf = pd.read_csv(chrSizeFile, sep='\t', header=None, names=['chromosome', 'chromosome_size'])
                pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['chrSizeFileName'] = chrSizeFile.filename;
                pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['chrSizeDf'] = chrSizeDf;
            else:
                return "Error : invalid chromosome size file", 400
        elif chrSizeFileStates[i] == 'conserved':
            chrSizeDf = pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['chrSizeDf']#.copy()
        elif chrSizeFileStates[i] == 'deleted':
            pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['chrSizeFileName'] = None;
            pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['chrSizeDf'] = None;
        #elif chrSizeFileStates[i] == 'none':

        bedDf = None
        if bedFile and bedFile.filename:
            if allowed_file(bedFile.filename):
                bedDf = pd.read_csv(bedFile, sep='\t', header=None, names=['chromosome', 'original_start', 'original_end', 'gene', 'score', 'strand'])
                if (bedDf['original_start'] >= bedDf['original_end']).any():
                    return "Error : in '" + bedFile.filename + "' a value in the 'start' column is equal to or greater than that in the 'end' column."
                bedDf['original_genome'] = originalGenomes[i]
                bedDf['renamed_genome'] = renamedGenomes[i]
                # Supprime la colonne 'score'
                bedDf = bedDf.drop(columns=['score'])

                if (chrSizeFile and chrSizeFile.filename) or chrSizeFileStates[i] == 'conserved':
                    # Fusionne les dataframes 'bedDf' et 'chrSizeDf'
                    bedDf = pd.merge(bedDf, chrSizeDf, how='left', on=['chromosome'])
                    #bedDf['chromosome_size'] = bedDf['chromosome_size'].astype('Int64')
                    bedDf[
                        ['is_real_chr_size', 'should_gene_be_displayed', 'flipped_start', 'flipped_end']] = bedDf.apply(
                        lambda row: pd.Series(
                            (False, False, None, None)
                            if pd.isna(row['chromosome_size'])
                            else (True, True, row['chromosome_size'] - row['original_end'], row['chromosome_size'] - row['original_start'])
                        ),
                        axis=1
                    )
                    #bedDf['flipped_start'] = bedDf['flipped_start'].astype('Int64')
                    #bedDf['flipped_end'] = bedDf['flipped_end'].astype('Int64')
                elif chrSizeFileStates[i] == 'deleted' or chrSizeFileStates[i] == 'none':
                    bedDf['is_real_chr_size'] = False
                    bedDf['should_gene_be_displayed'] = True
                    bedDf['chromosome_size'] = bedDf.groupby(['chromosome'])['original_end'].transform('max')
                    bedDf['flipped_start'] = bedDf['chromosome_size'] - bedDf['original_end']
                    bedDf['flipped_end'] = bedDf['chromosome_size'] - bedDf['original_start']

                pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedFileName'] = bedFile.filename;
                pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf'] = bedDf;
                bedDfs.append(bedDf)
            else:
                return "Error : invalid BED file", 400
        elif bedFileStates[i] == 'conserved':
            if chrSizeFile and chrSizeFile.filename:
                bedDf = pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf']#.copy()
                bedDf['renamed_genome'] = renamedGenomes[i]

                print(bedDf.columns)
                print(chrSizeDf)
                print(bedDf[['original_start', 'original_end', 'chromosome_size', 'flipped_start', 'flipped_end']])

                # Supprime la precedente colonne 'chromosome_size'
                bedDf = bedDf.drop(columns=['chromosome_size'])
                # Fusionne les dataframes 'bedDf' et 'chrSizeDf'
                bedDf = pd.merge(bedDf, chrSizeDf, how='left', on=['chromosome'])
                bedDf['chromosome_size'] = bedDf['chromosome_size'].astype('Int64')
                bedDf[['is_real_chr_size', 'should_gene_be_displayed', 'flipped_start', 'flipped_end']] = bedDf.apply(
                    lambda row: pd.Series(
                        (False, False, None, None)
                        if pd.isna(row['chromosome_size'])
                        else (True, True, row['chromosome_size'] - row['original_end'], row['chromosome_size'] - row['original_start'])
                    ),
                    axis=1
                )
                bedDf['flipped_start'] = bedDf['flipped_start'].astype('Int64')
                bedDf['flipped_end'] = bedDf['flipped_end'].astype('Int64')

                print(bedDf[['original_start', 'original_end', 'chromosome_size', 'flipped_start', 'flipped_end']])

                pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf'] = bedDf
            elif chrSizeFileStates[i] == 'conserved':
                bedDf = pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf']#.copy()
                bedDf['renamed_genome'] = renamedGenomes[i]
            elif chrSizeFileStates[i] == 'deleted':
                bedDf = pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf']#.copy()
                bedDf['renamed_genome'] = renamedGenomes[i]
                bedDf['is_real_chr_size'] = False
                bedDf['should_gene_be_displayed'] = True
                bedDf['chromosome_size'] = bedDf.groupby(['chromosome'])['original_end'].transform('max')
                bedDf['flipped_start'] = bedDf['chromosome_size'] - bedDf['original_end']
                bedDf['flipped_end'] = bedDf['chromosome_size'] - bedDf['original_start']
                #pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf'] = bedDf
            elif chrSizeFileStates[i] == 'none':
                bedDf = pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf']#.copy()
                bedDf['renamed_genome'] = renamedGenomes[i]
            bedDfs.append(bedDf)
        elif bedFileStates[i] == 'deleted':
            pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedFileName'] = None;
            pickleDict['bedChrSizeByOriginalGenomeDict'][originalGenomes[i]]['bedDf'] = None;
        #elif bedFileStates[i] == 'none':

    print('a')

    if len(bedDfs) >= 1:
        # Fusionne tous les dataframes de la liste 'bedDfs'
        mergedBedDf = pd.concat(bedDfs, ignore_index=True)
    else:
        mergedBedDf = pd.DataFrame(columns=['original_genome', 'renamed_genome', 'chromosome', 'chromosome_size', 'gene', 'strand', 'original_start', 'original_end', 'flipped_start', 'flipped_end', 'is_real_chr_size', 'should_gene_be_displayed'])

    # Supprime les genes qui ne doivent pas etre affiches
    mergedBedDf = mergedBedDf[mergedBedDf['should_gene_be_displayed'] == True]
    # Supprime la colonne should_gene_be_displayed
    mergedBedDf = mergedBedDf.drop(columns=['should_gene_be_displayed'])

    print('b')

    # Fusionne les dataframes TSV et BED
    mergedTsvBedDf = pd.merge(mergedBedDf, pickleDict['tsvDf'], how='left', on=['gene', 'original_genome'])
    # Remplace les orthogroupes manquant par un caractere espace
    mergedTsvBedDf = mergedTsvBedDf.fillna({'orthogroups': ' ', 'color_orthogroups': ' '})
    # A present 'mergedTsvBedDf' est de la forme ['original_genome', 'renamed_genome', 'chromosome', 'chromosome_size', 'gene', 'strand', 'original_start', 'original_end', 'flipped_start', 'flipped_end', 'orthogroups', 'color_orthogroups', 'is_real_chr_size']
    #print(mergedTsvBedDf)
    # Enregistre 'mergedTsvBedDf' dans le dictionnaire 'pickleDict'
    pickleDict['mergedTsvBedDf'] = mergedTsvBedDf;

    print('c')

    print(mergedTsvBedDf[['original_start', 'original_end', 'chromosome_size', 'flipped_start', 'flipped_end']])
    print(pickleDict)

    return(pickleDict)






    
    
# Lire le fichier 'tsv' et creer un objet FileStorage
with open(args['tsv'], "rb") as tsvFile:
    tsvFileContent = tsvFile.read()  # Lire tout le contenu du fichier dans la memoire
# Utiliser BytesIO pour creer un flux en memoire
tsvFileStream = BytesIO(tsvFileContent)
# Creer un objet FileStorage
tsvFileStorage = FileStorage(
    stream=tsvFileStream,      # Flux en memoire
    filename=args['tsv'],      # Nom du fichier
    content_type="text/plain"  # Type MIME du fichier
)
pickleDict = readTsvFile(tsvFileStorage)

csvDf = pd.read_csv(args['csv'], sep="\t", header=None, names=['name','bed_file','renamed', 'chromosome_size_file'])

originalGenomes = csvDf['name'].tolist()

bedFileStates = [None] * len(csvDf)

bedFileStorages = []
bedFiles = csvDf['bed_file'].tolist()
for i in range(len(bedFiles)):
    # Lire le fichier 'bed' et creer un objet FileStorage
    with open(bedFiles[i], "rb") as bedFile:
        bedFileContent = bedFile.read()  # Lire tout le contenu du fichier dans la memoire
    # Utiliser BytesIO pour creer un flux en memoire
    bedFileStream = BytesIO(bedFileContent)
    # Creer un objet FileStorage
    bedFileStorage = FileStorage(
        stream=bedFileStream,      # Flux en memoire
        filename=bedFiles[i],      # Nom du fichier
        content_type="text/plain"  # Type MIME du fichier
    )
    bedFileStorages.append(bedFileStorage)

renamedGenomes = csvDf['renamed'].tolist()

chrSizeFileStates = [None] * len(csvDf)

chrSizeFileStorages = []
chrSizeFiles = csvDf['chromosome_size_file'].tolist()
for i in range(len(chrSizeFiles)):
    # Lire le fichier 'chromosome size' et creer un objet FileStorage
    with open(chrSizeFiles[i], "rb") as chrSizeFile:
        chrSizeFileContent = chrSizeFile.read()  # Lire tout le contenu du fichier dans la memoire
    # Utiliser BytesIO pour creer un flux en memoire
    chrSizeFileStream = BytesIO(chrSizeFileContent)
    # Creer un objet FileStorage
    chrSizeFileStorage = FileStorage(
        stream=chrSizeFileStream,  # Flux en memoire
        filename=chrSizeFiles[i],  # Nom du fichier
        content_type="text/plain"  # Type MIME du fichier
    )
    chrSizeFileStorages.append(chrSizeFileStorage)

pickleDict = readBedChrSizeFile(originalGenomes, bedFileStates, bedFileStorages, renamedGenomes, chrSizeFileStates, chrSizeFileStorages, pickleDict)
        
with open(args['pickle_output'], 'wb') as pickleFile:
    pickle.dump(pickleDict, pickleFile)
    
    
    
    
    