from Bio import SeqIO
from Bio.Seq import Seq
import pandas as pd
import numpy as np
#pd.options.mode.copy_on_write = True 
import argparse
import sys
import re

# Recuperation des parametres
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--fasta", required=True, help="fasta file")
ap.add_argument("-g", "--gff", required=True, help="GFF file")
ap.add_argument("-fo", "--fasta_output", required=True, help="fasta output file")
ap.add_argument("-b4o", "--bed4_output", required=True, help="4 column bed output file")
ap.add_argument("-b6o", "--bed6_output", required=True, help="6 column bed output file")
ap.add_argument("-t", "--type", required=False, default='NCBI', help="input type (NCBI,BIPAA,Genoscope,other)")
ap.add_argument("-r", "--rename", required=False, default=None, help="correspondance file of chromosomes")
args = vars(ap.parse_args())

# Creation d'un dictionnaire qui contient les proteines avec la key = nom de la proteine a partir du fichier fasta
prots={}
size={} 
for index, record in enumerate(SeqIO.parse(args['fasta'], "fasta")):
    prots[record.id]=record

# Retourne la taille d'une proteine
def get_protlen(prot):
    if (prot in prots):
        return (len(prots[prot].seq))
    else:
        return(0)
    
# On ecrit le fasta dans le fichier fasta_output (passe en parametre)
def writeOutputBedFasta(gene_df, gene_rna_cds_df):
    # On recupere la taille de la proteine
    gene_rna_cds_df.loc[:,'prot_len']=gene_rna_cds_df['protein'].apply(get_protlen)
    gene_rna_cds_df=gene_rna_cds_df[gene_rna_cds_df.loc[:,'prot_len']>0]
    # On recupere tous les noms des genes restant apres la suppression des proteines incorrectes
    genes=gene_rna_cds_df['gene'].unique()
    # On ecrit les fichiers 'bed'
    gene_df.loc[gene_df['gene'].isin(genes),['scaffold', 'start', 'end', 'gene']].to_csv(args['bed4_output'], index=False,header=False, sep="\t")
    gene_df.loc[gene_df['gene'].isin(genes),['scaffold', 'start', 'end', 'gene', 'score','strand']].to_csv(args['bed6_output'], index=False,header=False, sep="\t")
    
    of=open(args['fasta_output'],'w')
    # On extrait la plus grande proteine pour chaque gene
    for gene in genes:
        # Extraction de la proteine la plus grande (on filtre dans le dataframe gene_rna_cds_df tous les transcripts correspondant au gene, on les trie par taille descendante et on prend le premier
        best=gene_rna_cds_df.loc[gene_rna_cds_df['gene']==gene, ['gene','protein', 'prot_len']].sort_values('prot_len', ascending=False).iloc[0]
        # prot : nom de la proteine
        prot=best['protein']
        # print(gene + " " + prot)
        # On recupere la proteine dans le dictionnaire prot
        rec=prots[prot]
        # On change son id par le nom du gene
        rec.id=gene
        # On enleve sa description
        rec.description=''
        # On l'ecrit dans le fichier
        SeqIO.write(rec, of, 'fasta')
    
# GENERATION DU FICHIER BED
# On lit le fichier gff a partir de pandas                        
gff_df=pd.read_csv(args['gff'], sep="\t",header=None,comment='#',usecols=[0,1,2,3,4,5,6,7,8],names=['scaffold','source','type', 'start','end', 'score','strand','frame','info'])

# Ajout d'une colonne 'gene' qui contient l'ID du gene
if (args['type'] == 'NCBI'):
    # On selectionne que les lignes de type 'gene'
    gene_df=gff_df.loc[gff_df['type']=='gene',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'biotype']=gene_df['info'].str.extract('gene_biotype=([^;]+)')
    gene_df=gene_df[gene_df['biotype']=='protein_coding']
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('gene=([^;]+)')
if ((args['type'] == 'BIPAA') | (args['type'] == 'Mathers') | (args['type'] == 'OX-heart') | (args['type'] == 'Helixer') | (args['type'] == 'Chinese') | (args['type'] == 'W03') | (args['type'] == 'Jzs') | (args['type'] == 'PCA') | (args['type'] == 'C1.3') | (args['type'] == 'XQC') | (args['type'] == 'ECD04') | (args['type'] == 'AiLiaoBai') | (args['type'] == 'R500')):
    # On selectionne que les lignes de type 'gene'
    gene_df=gff_df.loc[gff_df['type']=='gene',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('ID=([^;]+)')
if (args['type'] == 'Genoscope'):
    # On selectionne que les lignes de type 'mRNA'
    gene_df=gff_df.loc[gff_df['type']=='mRNA',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('ID=([^;]+)')
    gene_df.loc[:,'gene']=gene_df['gene'].str.replace('-R$', '')
if ((args['type'] == 'Braker') | (args['type'] == 'd134')):
    # On selectionne que les lignes de type 'transcript'
    gene_df=gff_df.loc[gff_df['type']=='transcript',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('gene_id=([^;]+)')
if (args['type'] == 'EnsEMBL'):
    # On selectionne que les lignes de type 'gene'
    gene_df=gff_df.loc[gff_df['type']=='gene',['scaffold', 'start', 'end', 'score','strand', 'info']]
    # On ne prends que les genes de 'protein_coding'
    gene_df.loc[:,'biotype']=gene_df['info'].str.extract('biotype=([^;]+)')
    gene_df=gene_df[gene_df['biotype']=='protein_coding']
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('ID=gene:([^;]+)')
if (args['type'] == 'Guo'):
    # On selectionne que les lignes de type 'gene'
    gene_df=gff_df.loc[gff_df['type']=='gene',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('ID=([^;]+)_')
if (args['type'] == 'C8'):
    # On selectionne que les lignes de type 'gene'
    gene_df=gff_df.loc[gff_df['type']=='gene',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('ID=([^;]+)')
    # On ne prends pas en compte les pseudo
    gene_df.loc[:,'pseudo']=gene_df['info'].str.extract('pseudo=([^;]+)')
    gene_df=gene_df[gene_df['pseudo']!='TRUE']
if ((args['type'] == 'ZYCX') | (args['type'] == 'Lembke')):
    # On selectionne que les lignes de type 'mRNA'
    gene_df=gff_df.loc[gff_df['type']=='mRNA',['scaffold', 'start', 'end', 'score','strand', 'info']]
    gene_df.loc[:,'gene']=gene_df['info'].str.extract('ID=([^;]+)')

# On compte la frequence de chaque gene
gene_counts=gene_df['gene'].value_counts()
# On conserve uniquement les genes qui sont presents UNE SEULE FOIS
gene_df=gene_df[gene_df['gene'].map(gene_counts)==1]

# Renommage des chromosomes avec le fichier de correspondance s'il existe    
if (args['rename'] != None):
    rename_df=pd.read_csv(args['rename'], header=None, names=('old_name', 'new_name'), sep =" ")
    # On fusionne le dataframe avec le dataframe gene_df et on cree une nouvelle colonne 'new_name' 
    gene_df=gene_df.merge(rename_df,left_on='scaffold', right_on='old_name')
    # On ne garde que les chromosomes qui sont presents dans le fichier de correspondance
    gene_df=gene_df[gene_df['scaffold'].isin(rename_df['old_name'].tolist())]
    # On remplace l'ancienne colonne 'scaffold' par la nouvelle colonne 'new_name'
    gene_df['scaffold']=gene_df['new_name']
    # On ne garde que les chromosomes qui sont presents dans le fichier de correspondance
    gff_df=gff_df[gff_df['scaffold'].isin(rename_df['old_name'].tolist())]

# GENERATION DU FICHIER FASTA
# On recupere les transcripts de chaque gene codant des proteines et on extrait la proteine la plus longue pour un gene 
if (args['type'] == 'NCBI'):
    cds_df=gff_df.loc[gff_df['type']=='CDS',:]
    cds_df.loc[:,'protein']=cds_df['info'].str.extract('protein_id=([^;]+)')
    cds_df.loc[:,'gene']=cds_df['info'].str.extract('gene=([^;]+)')
    writeOutputBedFasta(gene_df, cds_df)
    
if (args['type'] == 'EnsEMBL'):
    rna_df=gff_df.loc[gff_df['type']=='transcript',:]
    # On ne prends que les genes de 'protein_coding'
    rna_df.loc[:,'biotype']=rna_df['info'].str.extract('biotype=([^;]+)')
    rna_df=rna_df[rna_df['biotype']=='protein_coding']
    # On extrait le nom de la proteine : >Bo4g125260.1_chrC04_32280894_32281765
    rna_df=rna_df.merge(rename_df,left_on='scaffold', right_on='old_name')
    rna_df['scaffold']=rna_df['new_name']
    rna_df['protein']=np.where(rna_df['scaffold'].str.startswith('C'), rna_df['info'].str.extract('ID=transcript:([^;]+)', expand=False)+'_chr'+rna_df['scaffold']+'_'+rna_df['start'].values.astype(str)+'_'+rna_df['end'].values.astype(str), rna_df['info'].str.extract('ID=transcript:([^;]+)', expand=False)+'_'+rna_df['scaffold']+'_'+rna_df['start'].values.astype(str)+'_'+rna_df['end'].values.astype(str))
    # On recupere le nom du gene
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('PARENT=gene:([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)

if ((args['type'] == 'Chinese') | (args['type'] == 'Guo')):
    cds_df=gff_df.loc[gff_df['type']=='CDS',:]
    cds_df.loc[:,'protein']=cds_df['info'].str.extract('Protein_Accession=([^;]+)')
    cds_df.loc[:,'gene']=cds_df['info'].str.extract('Parent=([^;]+)\.\d')
    writeOutputBedFasta(gene_df, cds_df)

if (args['type'] == 'C8'):
    cds_df=gff_df.loc[gff_df['type']=='CDS',:]
    # On ne prends pas en compte les pseudo
    cds_df.loc[:,'pseudo']=cds_df['info'].str.extract('pseudo=([^;]+)')
    cds_df=cds_df[cds_df['pseudo']!='TRUE']
    cds_df.loc[:,'protein']=cds_df['info'].str.extract('Protein_Accession=([^;]+)')
    cds_df.loc[:,'gene']=cds_df['info'].str.extract('Parent=([^;]+)\.\d')
    writeOutputBedFasta(gene_df, cds_df)

if (args['type'] == 'Braker'):
    rna_df=gff_df.loc[gff_df['type']=='transcript',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('transcript_id=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('gene_id=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)
        
if (args['type'] == 'BIPAA'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['protein'].str.extract('(.+)-R[A-Z]')
    writeOutputBedFasta(gene_df, rna_df)
        
if (args['type'] == 'Genoscope'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'protein']=rna_df['protein'].str.replace('-R$', '', regex=True)
    rna_df.loc[:,'gene']=rna_df['protein']
    writeOutputBedFasta(gene_df, rna_df)

if (args['type'] == 'Mathers'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'protein']=rna_df['protein'].str.replace('\.t([0-9]+)$', lambda m: '.p'+m.group(1), regex=True)
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('Parent=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)
        
if ((args['type'] == 'OX-heart') | (args['type'] == 'd134') | (args['type'] == 'Helixer') | (args['type'] == 'XQC')):
    cds_df=gff_df.loc[gff_df['type']=='CDS',:]
    cds_df.loc[:,'protein']=cds_df['info'].str.extract('Parent=([^;]+)')
    cds_df.loc[:,'gene']=cds_df['protein'].str.replace('\.\d+$', '', regex=True)
    writeOutputBedFasta(gene_df, cds_df)

if (args['type'] == 'W03'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['protein'].str.replace('mRNA.', '', regex=True)
    writeOutputBedFasta(gene_df, rna_df)
                
if (args['type'] == 'Jzs'):
    gene2_df=gff_df.loc[gff_df['type']=='gene',:]
    gene2_df.loc[:,'protein']=gene2_df['info'].str.extract('ID=([^;]+)')
    gene2_df.loc[:,'gene']=gene2_df['info'].str.extract('ID=([^;]+)')
    writeOutputBedFasta(gene_df, gene2_df)
        
if (args['type'] == 'PCA'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('Parent=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('Parent=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)

if (args['type'] == 'C1.3'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']='Parent='+rna_df['info'].str.extract('Parent=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('Parent=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)

if (args['type'] == 'ECD04'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('Parent=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)

if (args['type'] == 'AiLiaoBai'):
    cds_df=gff_df.loc[gff_df['type']=='CDS',:]
    cds_df.loc[:,'protein']=cds_df['info'].str.extract('Protein_Accession=([^;]+)')
    cds_df.loc[:,'gene']=cds_df['info'].str.extract('Parent=([^;]+)')+'-gene'
    writeOutputBedFasta(gene_df, cds_df)

if (args['type'] == 'ZYCX'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['protein']
    writeOutputBedFasta(gene_df, rna_df)
    
if (args['type'] == 'Lembke'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']='Parent='+rna_df['info'].str.extract('ID=([^;]+)')
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('ID=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)

if (args['type'] == 'R500'):
    rna_df=gff_df.loc[gff_df['type']=='mRNA',:]
    rna_df.loc[:,'protein']=rna_df['info'].str.extract('Name=([^;]+)')+'.p'
    rna_df.loc[:,'gene']=rna_df['info'].str.extract('Parent=([^;]+)')
    writeOutputBedFasta(gene_df, rna_df)
